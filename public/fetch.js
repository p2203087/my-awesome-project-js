/*
 * fonction pour mettre les images dans la zone vos photos depuis l'api pexels
*/
async function fetchImages() {
    try {
        // Récupérer les images depuis l'API Pexels
        let response = await fetch("https://api.pexels.com/v1/search?query=sport", {
            headers: {
                Authorization: 'yHNAMyyARc0Rc2lEbccqfYxeiiLvxnSDyFfODdSSOlQwbohbPT9iiTgu',
                Accept: 'application/json'
            }
        });

        // Vérifier si la requête API a réussi
        if (response.ok) {
            // Analyser la réponse JSON
            let data = await response.json();

            // Obtenir le tableau d'images de la réponse
            let images = data.photos;

            // Obtenir l'élément conteneur où les images seront affichées
            let imageContainers = document.getElementById("photo");


            // Vérifier si le nombre d'images récupérées est insuffisant
            if (images.length < 5) {
                throw new Error('Erreur : Pas assez d\'images disponibles.');
            }
            let tabImage = [];
            // Parcourir les 5 premières images
            for (let indiceImage = 0; indiceImage < 5; indiceImage++) {
                // genere un nombre aleatoire
                let random = Math.floor(Math.random() * images.length);
                //genere l'image avec le nombre aleatoire
                let photoData = images[random];
                // si l'image existe deja on recommence la boucle
                if (tabImage.includes(photoData)) {
                    indiceImage--;
                    continue;
                }
                //on ajoute l'image au tableaux d'image
                tabImage.push(photoData);
                let imgElement = document.createElement('img');

                // Définir la source de l'élément img sur l'URL de l'image originale
                imgElement.src = photoData.src.original;

                // Définir la largeur et la hauteur maximales de l'élément img à 80 pixels
                imgElement.style.maxWidth = '80px';
                imgElement.style.maxHeight = '80px';

                // Ajouter l'élément img au conteneur
                imageContainers.appendChild(imgElement);
            }
        } else {
            // Gérer les erreurs si la requête API n'a pas réussi
            throw new Error('Erreur : ' + response.status);
        }
    } catch (err) {
        // Journaliser les erreurs qui se produisent pendant le processus
        console.log(err);
    }
}

// Appeler la fonction fetchImages pour récupérer et afficher les images
fetchImages();


/**
 * Gestionnaire d'événements pour des clics sur l'élément 'poster'.
 * Permet de sélectionner et désélectionner des zones en cliquant dessus,
 * et réinitialise la sélection si on clique en dehors des zones.
 */
var poster = document.getElementById('poster');
// Variable pour suivre la dernière zone cliquée
var lastClicked = null;

/**
 * Ajoute un écouteur d'événements de clic sur l'élément 'poster'.
 * 
 * @param {Event} e - L'événement de clic.
 */
poster.addEventListener('click', (e) => {
    // Récupérer la zone cliquée, en prenant en compte les clics sur les images
    let clickedZone = e.target;
    if (e.target.tagName === 'IMG') {
        // Si c'est une image, obtenir le parent qui est la zone
        clickedZone = e.target.parentNode;
    }

    // Vérifie si l'élément cliqué est une zone
    if (clickedZone.id && clickedZone.id.startsWith('zone')) {
        console.log(clickedZone.id + ' clicked');

        // Réinitialise le style de la dernière zone cliquée si elle est différente de l'actuelle
        if (lastClicked && lastClicked !== clickedZone) {
            lastClicked.style.border = '1px solid black';
        }

        // Change le style de la zone cliquée et met à jour 'lastClicked'
        if (lastClicked !== clickedZone) {
            clickedZone.style.border = '2px solid red';
            lastClicked = clickedZone;
        } else {
            clickedZone.style.border = '1px solid black';
            lastClicked = null;
        }
    } else {
        // Réinitialiser toutes les zones si on clique en dehors
        console.log('ggez');
        ['zone1', 'zone2', 'zone3', 'zone4', 'zone5'].forEach(zoneId => {
            var zone = document.getElementById(zoneId);
            if (zone) {
                zone.style.border = '1px solid black';
            }
        });
        lastClicked = null;
    }
});


/**
 * Gestionnaire d'événements pour des clics sur les images.
 * Permet de sélectionner des images en cliquant dessus,
 * quand une zone est deja selectionner.
 */
var imageContainer = document.getElementById('photo');

/**
 * Ajoute un écouteur d'événements de clic sur les images.
 * 
 * @param {Event} e - L'événement de clic.
 */
imageContainer.addEventListener('click', (e) => {
    if (e.target.tagName === 'IMG') {
        if (lastClicked) {
            let clonedImage = e.target.cloneNode(true);

            // met la taille de l'image a 250 * 300
            clonedImage.style.maxWidth = '150%';
            clonedImage.style.maxHeight = '100%';

            // regarde si la zone conient deja une image
            if (lastClicked.children.length > 0) {
                // enleve l'ancienne image.
                lastClicked.removeChild(lastClicked.children[0]);
            }

            // ajoute la nouvelle image
            lastClicked.appendChild(clonedImage);

            // enleve le style pour chaque zone
            ['zone1', 'zone2', 'zone3', 'zone4', 'zone5'].forEach(zoneId => {
                var zone = document.getElementById(zoneId);
                if (zone) {
                    zone.style.border = '1px solid black';
                }
            });

            lastClicked = null;
        }
    }
});
